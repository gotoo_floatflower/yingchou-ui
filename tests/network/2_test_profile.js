const profile = require('../../src/network/profile');
const helper = require('../helper/auth');

const chai = require('chai');
const expect = chai.expect;
const httpStatus = require('quix/http').status;

describe('Test Auth', () => {

    it('can fetch platform user profile', (done) => {

        helper.loginPlatform()
            .then(() => {
                return profile.platformUser()
                    .then((response) => {
                        response.status.should.equal(httpStatus.OK);
                        done();
                    });
            })

    });

    it('can fetch manage profile', (done) => {
        helper.loginManager()
            .then(() => {
                return profile.manager()
                    .then((response) => {
                        response.status.should.equal(httpStatus.OK);
                        done();
                    })
            });
    });

    it('can fetch manager user profile', (done) => {
        helper.loginManager()
            .then(() => {
                return profile.managerUser()
                    .then((response) => {
                        response.status.should.equal(httpStatus.OK);
                        done();
                    })
            });
    });

    it('can fetch agent profile', (done) => {
        helper.loginAgent()
            .then(() => {
                return profile.agent()
                    .then((response) => {
                        response.status.should.equal(httpStatus.OK);
                        done();
                    })
            })
    });

    it('can fetch player profile', (done) => {
        helper.loginPlayer()
            .then(() => {
                return profile.player()
                    .then((response) => {
                        response.status.should.equal(httpStatus.OK);
                        done();
                    })
            })
    });

});