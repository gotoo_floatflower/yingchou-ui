const auth = require('../../src/network/auth');
const userRole = require('../../src/enum/user-role');

const chai = require('chai');
const expect = chai.expect;
const httpStatus = require('quix/http').status;

describe('Test Auth', () => {

    it('can login with platform user', (done) => {

        auth.platform({
            username: 'p_user1',
            password: '12345'
        })
        .then((results) => {
            expect(results.data).to.have.property('token');
            results.status.should.equal(httpStatus.OK);
            done();
        })

    })

    it('can login with manager user', (done) => {

        auth.manager({
            username: 'm_user1',
            password: '12345'
        })
        .then((results) => {
            expect(results.data).to.have.property('token');
            results.status.should.equal(httpStatus.OK);
            done();
        })

    })

    it('can login with agent', (done) => {

        auth.agent({
            username: 'a_user1',
            password: '12345'
        })
            .then((results) => {
                expect(results.data).to.have.property('token');
                results.status.should.equal(httpStatus.OK);
                done();
            })

    })

    it('can login with player', (done) => {

        auth.player({
            username: 'player1',
            password: '12345'
        })
            .then((results) => {
                expect(results.data).to.have.property('token');
                results.status.should.equal(httpStatus.OK);
                done();
            })

    })

});