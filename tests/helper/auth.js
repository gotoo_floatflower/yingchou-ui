const auth = require('../../src/network/auth');
const userRole = require('../../src/enum/user-role');

exports.loginPlatform = () => {
    return auth.platform({
        username: 'p_user1',
        password: '12345'
    });
};

exports.loginManager = () => {
    return auth.manager({
        username: 'm_user1',
        password: '12345'
    });
};

exports.loginAgent = () => {
    return auth.agent({
        username: 'a_user1',
        password: '12345'
    });
};

exports.loginPlayer = () => {
    return auth.player({
        username: 'player1',
        password: '12345'
    });
}

