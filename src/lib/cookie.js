exports.get = (key) => {

    let name = key + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i < ca.length; i ++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;

};

exports.set = (key, value, expiration) => {

    let d = new Date();
    if(expiration) {
        d.setTime(d.getTime() + (expiration * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = key + "=" + value + ";" + expires + ";path=/";
    } else {
        document.cookie = key + "=" + value + ";path=/";
    }
};