const env = require('./environment');

module.exports = (api) => {
    let ssl = env.get('SSL_ENABLED') === 'true';
    let serverUrl = env.get('SERVER_URL') || 'localhost:3000';
    return ssl ? `https://${serverUrl}${api}` : `http://${serverUrl}${api}`;
}