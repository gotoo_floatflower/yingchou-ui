exports.get = (envName) => {
    let env = process.env[`VUE_APP_${envName}`];
    if(typeof env === 'undefined') return null;
    else {
        if(env === 'false') return false;
        else if (env === 'true') return true;
        else return env;
    }
};