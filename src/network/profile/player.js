const axios = require('axios');

const cookie = require('../../lib/cookie');
const urlHandler = require('../../lib/url-handler');

module.exports = () => {

    let authToken = cookie.get('auth_token');

    if(authToken) {
        return new Promise((resolve, reject) => {

            return axios.get(urlHandler('/v1/r'), {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            })
                .then(resolve, reject);

        });
    } else {

        return Promise.reject();

    }

};