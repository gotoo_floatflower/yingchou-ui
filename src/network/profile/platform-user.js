const axios = require('axios');

const cookie = require('../../lib/cookie');
const urlHandler = require('../../lib/url-handler');

module.exports = () => {

    let authToken = cookie.get('auth_token');

    if(authToken) {
        return new Promise((resolve, reject) => {

            return axios.get(urlHandler('/v1/pu'), {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            })
            .then((response) => {
                resolve(response);
            }, reject);

        });
    } else {

        return Promise.reject();

    }

};