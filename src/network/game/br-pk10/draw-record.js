const axios = require('axios');

const urlHandler = require('../../../lib/url-handler');
const cookie = require('../../../lib/cookie');

module.exports = (brPk10GameId, page = 1, limit = 5) => {

    let authToken = cookie.get('auth_token');

    if(authToken) {

        return new Promise((resolve, reject) => {

            return axios.get(urlHandler(`/v1/game/br-pk10/${brPk10GameId}/draw-records?page=${page}&limit=${limit}`), {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            })
            .then(resolve, reject);

        });

    } else {
        return Promise.reject();
    }
}