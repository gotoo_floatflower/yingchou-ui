const axios = require('axios');

const urlHandler = require('../../../lib/url-handler');
const cookie = require('../../../lib/cookie');

module.exports = (payload, brPk10GameId) => {
    let authToken = cookie.get('auth_token');

    if(authToken) {
        return new Promise((resolve, reject) => {
            if(Array.isArray(payload)) {

                let data = [];
                payload.map((p) => {

                    if(typeof p.bet && typeof p.amount) {
                        data.push({
                            bet: p.bet,
                            amount: p.amount
                        });
                    }

                });

                return axios.post(urlHandler(`/v1/game/br-pk10/${brPk10GameId}/bet`), data, {
                    headers: {
                        headers: {
                            Authorization: `Bearer ${cookie.get('auth_token')}`
                        }
                    }
                })
                    .then(resolve, reject);


            } else {
                reject();
            }
        })
    }
    else {
        return Promise.reject();
    }

};