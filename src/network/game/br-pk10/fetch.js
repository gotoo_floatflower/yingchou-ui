const axios = require('axios');

const urlHandler = require('../../../lib/url-handler');
const cookie = require('../../../lib/cookie');

module.exports = (gameId) => {

    let authToken = cookie.get('auth_token');

    if(authToken) {

        return new Promise((resolve, reject) => {

            return axios.get(urlHandler(`/v1/game/br-pk10/${gameId}`), {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            })
                .then(resolve, reject);

        })

    } else {
        return Promise.reject();
    }

};