exports.platform = require('./platform');
exports.manager = require('./manager');
exports.agent = require('./agent');
exports.player = require('./player');
exports.logout = require('./logout');