const axios = require('axios');

const cookie = require('../../lib/cookie');
const urlHandler = require('../../lib/url-handler');
const userRole = require('../../enum/user-role');

module.exports = (payload = {}) => {
    return new Promise((resolve, reject) => {

        return axios.post(urlHandler('/auth/m/login'), payload)
            .then((response) => {

                cookie.set('auth_token', response.data.token, 0);
                resolve(response);

            }, reject);
    })

};