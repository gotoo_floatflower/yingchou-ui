const axios = require('axios');
const urlHandler = require('../../lib/url-handler');
const cookie = require('../../lib/cookie');

module.exports = () => {

    return new Promise((resolve, reject) => {

        return axios.get(urlHandler('/auth/logout'), {
            headers: {
                Authorization: `Bearer ${cookie.get('auth_token')}`
            }
        })
            .then((response) => {

                resolve(response);

            }, reject)

    });

};
