const axios = require('axios');

const urlHandler = require('../../lib/url-handler');
const userRole = require('../../enum/user-role');
const cookie = require('../../lib/cookie');
const env = require('../../lib/environment');

module.exports = (role) => {

    let authToken = cookie.get('auth_token');

    if(authToken) {
        return new Promise((resolve, reject) => {

            let api = '';
            switch (role) {
                case userRole.PLATFORM:
                    api = '/v1/pu/ws';
                    break;
                case userRole.DIRECTOR:
                    break;
                case userRole.MANAGER:
                    api = '/v1/mu/ws';
                    break;
                case userRole.AGENT:
                    api = '/v1/a/ws';
                    break;
                case userRole.PLAYER:
                    api = '/v1/r/ws';
                    break;
            }
            return axios
                .get(urlHandler(api), {
                    headers: {
                        Authorization: `Bearer ${authToken}`
                    }
                })
                .then((response) => {
                    let ssl = env.get('SSL_ENABLED') || false;
                    let url = env.get('SERVER_URL') || 'localhost:3000';
                    let socketUrl = ssl ? `wss://${url}/ws/${response.data.token}` : `ws://${url}/ws/${response.data.token}`;
                    resolve(new WebSocket(socketUrl));
                }, reject)

        })
    } else {
        return Promise.resolve();
    }

};